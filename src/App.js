import {BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";

import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";

function App() {
  return (
    <Router>
      <div className="navigation">
        <Link to="/">Home</Link>
        <Link to="/profile">Profile</Link>
        <Link to ="/contact-us">Contact Us</Link>
    </div>

    <Switch>
      <Route component ={Home} path="/" exact={true}/>
      <Route component ={Profile} path="/profile"/>
      <Route component ={ContactUs} path="/contact-us"/>
    </Switch>


  
    </Router>
    
  )
}

export default App

